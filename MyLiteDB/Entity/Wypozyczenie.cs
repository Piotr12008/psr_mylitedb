﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLiteDB.Entity
{
    public class Wypozyczenie
    {
        public int Id { get; set; }
        public int Ksiazka { get; set; }
        public int Klient { get; set; }
        public DateTime DataWyp { get; set; }
        public DateTime DataOdda { get; set; }
        public bool Oddane { get; set; }

        public Wypozyczenie(String Ksiazka, String Klient, DateTime DataWyp)
        {
            this.Ksiazka = int.Parse(Ksiazka);
            this.Klient = int.Parse(Klient);
            this.DataWyp = DataWyp;
            this.Oddane = false;
        }
        public Wypozyczenie()
        { }
    }
}
