﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLiteDB.Entity
{
    public class Ksiazka
    {
        public int Id { get; set; }
        public string Tytul { get; set; }
        public String Autorzy { get; set; }
        public string ISBN { get; set; }
        public bool Dostepna { get; set; }

        public Ksiazka(string Tytul, string Autorzy, string ISBN)
        {
            this.Tytul = Tytul;
            this.Autorzy = Autorzy;
            this.ISBN = ISBN;
            this.Dostepna = true;
        }
        public Ksiazka()
        { }
    }
}
