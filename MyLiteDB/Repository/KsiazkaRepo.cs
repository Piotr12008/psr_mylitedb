﻿using LiteDB;
using MyLiteDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLiteDB.Repository
{
    public class KsiazkaRepo
    {
        public LiteDatabase DB;
        public KsiazkaRepo(LiteDatabase Database)
        {
            this.DB = Database;
        }
        public bool Save(Ksiazka ksiazka)
        {
            try
            {
                var collection = DB.GetCollection<Ksiazka>("Ksiazka");
                collection.Insert(ksiazka);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<Ksiazka> FindAll()
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            return collection.FindAll().ToList();
        }
        public Ksiazka FindOne(String idS)
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            Ksiazka ksiazka = new Ksiazka();
            try
            {
                int id = int.Parse(idS);
                ksiazka = collection.Find(ks => ks.Id == id).First();
            }
            catch (Exception)
            {
                return null;
            }
            return ksiazka;
        }
        public List<Ksiazka> FindAllAvailable()
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            List<Ksiazka> ksiazki = new List<Ksiazka>();
            try
            {
                ksiazki = collection.Find(ks => ks.Dostepna == true).ToList();
            }
            catch (Exception)
            {
                return ksiazki;
            }
            return ksiazki;
        }
        public Ksiazka FindOne(Ksiazka ksiazka)
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            Ksiazka k = new Ksiazka();
            try
            {
                k = collection.Find(ks => ks.Tytul.Equals(ksiazka.Tytul)).First();
            }
            catch (Exception)
            {
                return null;
            }
            return k;
        }
        public bool Update(String idS, Ksiazka ksiazka)
        {
            try
            {
                int id = int.Parse(idS);
                var collection = DB.GetCollection<Ksiazka>("Ksiazka");
                Ksiazka aktualizowany = collection.Find(cl => cl.Id == id).First();
                if (!ksiazka.Tytul.Equals(""))
                    aktualizowany.Tytul = ksiazka.Tytul;
                if (!ksiazka.Autorzy.Equals(""))
                    aktualizowany.Autorzy = ksiazka.Autorzy;
                if (!ksiazka.ISBN.Equals(""))
                    aktualizowany.ISBN = ksiazka.ISBN;

                collection.Update(aktualizowany);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Remove(String idS)
        {
            try
            {
                var collection = DB.GetCollection<Ksiazka>("Ksiazka");
                int id = int.Parse(idS);
                collection.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
