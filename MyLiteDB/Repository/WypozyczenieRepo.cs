﻿using LiteDB;
using MyLiteDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLiteDB.Repository
{
    public class WypozyczenieRepo
    {
        public LiteDatabase DB;
        public WypozyczenieRepo(LiteDatabase Database)
        {
            this.DB = Database;
        }
        public bool Save(Wypozyczenie wypozyczenie)
        {
            try
            {
                var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
                collection.Insert(wypozyczenie);
                KsiazkaRepo ksiazkaRepo = new KsiazkaRepo(DB);

                var collection2 = DB.GetCollection<Ksiazka>("Ksiazka");
                Ksiazka ksiazka = collection2.Find(ks => ks.Id == wypozyczenie.Ksiazka).First();
                ksiazka.Dostepna = false;
                collection2.Update(ksiazka);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Zwrot(Wypozyczenie wypozyczenie)
        {
            try
            {
                var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
                Wypozyczenie w = collection.Find(wyp => wyp.Id == wypozyczenie.Id).First();
                w.DataOdda = DateTime.Now;
                w.Oddane = true;
                collection.Update(w);

                var collection2 = DB.GetCollection<Ksiazka>("Ksiazka");
                Ksiazka ksiazka = collection2.Find(ks => ks.Id == wypozyczenie.Ksiazka).First();
                ksiazka.Dostepna = true;
                collection2.Update(ksiazka);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<Wypozyczenie> FindAll(String idS)
        {
            var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
            int id = int.Parse(idS);
            return collection.Find(wyp => wyp.Klient == id).ToList().OrderBy(s => s.Oddane).ToList();
        }
        public List<Wypozyczenie> FindAllNow(String idS)
        {
            int id = int.Parse(idS);
            var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
            return collection.Find(wyp => wyp.Klient == id && wyp.Oddane == false).ToList();
        }
        public Wypozyczenie FindOne(String idS)
        {
            var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
            Wypozyczenie wypozyczenie = new Wypozyczenie();
            try
            {
                int id = int.Parse(idS);
                wypozyczenie = collection.Find(wyp => wyp.Id == id).First();
            }
            catch (Exception)
            {
                return null;
            }
            return wypozyczenie;
        }
    }
}
