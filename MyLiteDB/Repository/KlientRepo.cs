﻿using LiteDB;
using MyLiteDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLiteDB.Repository
{
    public class KlientRepo
    {
        public LiteDatabase DB;
        public KlientRepo(LiteDatabase Database)
        {
            this.DB = Database;
        }
        public bool Save(Klient klient)
        {
            try
            {
                var collection = DB.GetCollection<Klient>("Klient");
                collection.Insert(klient);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<Klient> FindAll()
        {
            var collection = DB.GetCollection<Klient>("Klient");
            return collection.FindAll().ToList();
        }
        public Klient FindOne(String idS)
        {
            int id = int.Parse(idS);
            var collection = DB.GetCollection<Klient>("Klient");
            Klient client = new Klient();
            try
            {
                client = collection.Find(cl => cl.Id == id).First();
            }
            catch (Exception)
            {
                return null;
            }
            return client;
        }
        public bool Update(String idS, Klient klient)
        {
            try
            {
                int id = int.Parse(idS);
                var collection = DB.GetCollection<Klient>("Klient");
                Klient aktualizowany = collection.Find(cl => cl.Id == id).First();
                if (!klient.Imie.Equals(""))
                    aktualizowany.Imie = klient.Imie;
                if (!klient.Nazwisko.Equals(""))
                    aktualizowany.Nazwisko = klient.Nazwisko;

                collection.Update(aktualizowany);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Remove(String idS)
        {
            try
            {
                int id = int.Parse(idS);
                var collection = DB.GetCollection<Klient>("Klient");
                collection.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
